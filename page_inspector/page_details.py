# -*- coding: utf-8 -*-
import requests
from BeautifulSoup import BeautifulSoup

class PageDetails:
    """Gets information about a HTML page, based on an URL"""

    def __init__(self, url):
        response = requests.get(url)
        self.soup = BeautifulSoup(response.text)

    def meta_tags(self):
        """Return the list of meta tags present on URL"""
        tags = self.soup.findAll('meta')
        #TODO
        raise NotImplementedError()

    def page_title(self):
        return self.soup.title

    def word_count(self):
        raise NotImplementedError()

    def unique_words(self):
        """Return a list of unique words"""
        raise NotImplementedError()

    def non_unique_words(self):
        """Return a list of non unique words"""
        raise NotImplementedError()

    def common_words(self, num=5):
        """Return a list of the top `num` common words"""
        raise NotImplementedError()
