from django.test import TestCase
from page_inspector.page_details import PageDetails

class PageInspectorTesCase(TestCase):

    def test_get_page_details(self):
        pd = PageDetails('https://example.org/')

        self.assertTrue('charset' in pd.meta_tags())

        self.assertTrue('Example Domain' == pd.page_title())
        self.assertTrue(30 == pd.word_count())
        self.assertTrue(24 == pd.unique_words())
        self.assertTrue(6 == pd.non_unique_words())
        self.assertTrue('this' in pd.common_words(5))
