# -*- coding: utf-8 -*-

from django import forms
from django.core.validators import URLValidator


class PageInspectorForm(forms.Form):
    url = forms.URLField(label=u'url', validators=[URLValidator])
