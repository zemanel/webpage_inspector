# -*- encoding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from page_inspector.forms import PageInspectorForm
from page_inspector.page_details import PageDetails

def page_details_view(request):
    ctx = {}
    if request.POST:
        form = PageInspectorForm(request.POST)
        if form.is_valid():
            ctx['page_details'] = PageDetails(form.cleaned_data['url'])
    else:
        form = PageInspectorForm()
    ctx['form'] = form
    return render_to_response('index.html', ctx,
                              context_instance=RequestContext(request))
