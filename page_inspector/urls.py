# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from page_inspector.views import page_details_view

urlpatterns = patterns('',
    url(r'^$', page_details_view),
)